const form = document.querySelector("form");


  eField = form.querySelector(".email"),
eInput = eField.querySelector("input"),
pField = form.querySelector(".password"),
pInput = pField.querySelector("input"),
nField = form.querySelector(".name"),
nInput = nField.querySelector("input"),
numField = form.querySelector(".number"),
numInput = numField.querySelector("input");


// document.getElementById("add").style.visibility="hidden";

  

  form.onsubmit = (e)=>{
  
  e.preventDefault(); 

  (eInput.value == "") ? eField.classList.add("error") : checkEmail();
  (pInput.value == "") ? pField.classList.add("error") : checkPass();
  (nInput.value == "") ? nField.classList.add("error") : checkName();
  (numInput.value == "") ? numField.classList.add("error") : checkNumber();

  // alert(document.getElementById("onclick").value);

 

  eInput.onkeyup = ()=>{checkEmail();} 
  pInput.onkeyup = ()=>{checkPass();} 
  nInput.onkeyup = ()=>{checkName();}
  numInput.onkeyup = ()=>{checkNumber();}

  function checkEmail(){ 
    // let pattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/; 
    const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(!eInput.value.match(pattern)){ 
      eField.classList.add("error");
      eField.classList.remove("valid");
      let errorTxt = eField.querySelector(".error-txt");
      (eInput.value != "") ? errorTxt.innerText = "Enter a valid email address" : errorTxt.innerText = "Email can't be blank";
    }else{ 
      eField.classList.remove("error");
      eField.classList.add("valid");
    }
  }

  function checkNumber(){ 
    var pattern = /^\d{10}$/;
    if(!numInput.value.match(pattern)){ 
      numField.classList.add("error");
      numField.classList.remove("valid");
      let errorTxt = numField.querySelector(".error-txt");
     
      (numInput.value != "") ? errorTxt.innerText = "Enter a valid Number address" : errorTxt.innerText = "Number can't be blank";
    }else{ 
      numField.classList.remove("error");
      numField.classList.add("valid");
    }
  }

  function checkPass(){ 
    if(pInput.value == ""){ 
      pField.classList.add("error");
      pField.classList.remove("valid");
    }else{ 
      pField.classList.remove("error");
      pField.classList.add("valid");
    }
  }
  function checkName() {
    var pattern = /^[A-z ]+$/;
     // /^[A-z ]+$/
           if (!nInput.value.match(pattern)) {
               nField.classList.add("error");
               nField.classList.remove("valid");
               let errorTxt = nField.querySelector(".error-txt");
   
               (nInput.value != "") ? errorTxt.innerText = "Enter a valid Name" : errorTxt.innerText = "Name can't be blank";
           } else {
               nField.classList.remove("error");
               nField.classList.add("valid");
           }
       }

  
  if(!eField.classList.contains("error") && !pField.classList.contains("error")&& !nField.classList.contains("error") && !numField.classList.contains("error")){
      
    //   if(document.getElementById("add_submit").value==="Add Data")
    if(document.getElementById("add_submit"))
      {
                      alert(" Inside Add Module")
                      fetch('http://localhost:3000/add', {
                                            method: 'POST', 
                                            headers: {
                                            'Content-Type': 'application/json',
                                            },
                                            body: JSON.stringify({
                                                name :   document.getElementById("name").value,
                                            email:  document.getElementById("email").value,
                                            password :  document.getElementById("password").value,
                                            mobile :document.getElementById("mobile").value}),
                                            })
                                            .then(response => response.json())
                                            .then(data => {console.log('Success:', data);})
                                            .catch((error) => {console.error('Error:', error);
                                        });
                                        location.replace("index.html")
          }
      
      else
      {

        alert("Inside Upade MOdule");
        url = window.location.href;
        id = (new URL(url)).searchParams;
        customer_id = id.get('id');
        
        const d = {
            customer_id: customer_id,
            name: document.getElementById("name").value,
            email: document.getElementById("email").value,
            password: document.getElementById("password").value,
            mobile: document.getElementById("mobile").value
        }


        fetch('http://localhost:3000/update', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(d)
        })
            .then(response => response.json())
            .then(data => {

                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });

            alert("Succefully Update ");
           
        location.replace("index.html")

        // window.location.href = form.getAttribute("action"); //redirecting user to the specified url which is inside action attribute of form tag
    }
      }
         
    }



